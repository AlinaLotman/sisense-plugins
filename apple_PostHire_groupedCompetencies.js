/*
Welcome to your Widget's Script.

To learn how you can access the Widget and Dashboard objects, see the online documentation at http://developer.sisense.com/pages/viewpage.action?pageId=557127
*/


var competencyNames=[];
competencyNames.push({"Name":"TOOLS","Color":"#395D8D"});
competencyNames.push({"Name":"PRODUCTS","Color":"#395D8D"});
competencyNames.push({"Name":"PROCEDURES","Color":"#395D8D"});

competencyNames.push({"Name":"DEMONSTRATES RESILIENCE","Color":"#5C8ECA"});
competencyNames.push({"Name":"HAS PATIENCE","Color":"#5C8ECA"});
competencyNames.push({"Name":"STAYS COMPOSED","Color":"#5C8ECA"});
competencyNames.push({"Name":"TOLERATES STRESS","Color":"#5C8ECA"});
competencyNames.push({"Name":"IS OPEN TO FEEDBACK","Color":"#5C8ECA"});

competencyNames.push({"Name":"EDUCATES CUSTOMERS","Color":"#7B93A2"});
competencyNames.push({"Name":"FOCUSES ON CUSTOMERS","Color":"#7B93A2"});
competencyNames.push({"Name":"READS CUSTOMERS","Color":"#7B93A2"});
competencyNames.push({"Name":"UNDERSTANDS DIVERSITY","Color":"#7B93A2"});
competencyNames.push({"Name":"BEHAVES COURTEOUSLY","Color":"#7B93A2"});
competencyNames.push({"Name":"BEHAVES TACTFULLY","Color":"#7B93A2"});
competencyNames.push({"Name":"LISTENS","Color":"#7B93A2"});
competencyNames.push({"Name":"MAINTAINS APPROPRIATE TONE","Color":"#7B93A2"});
competencyNames.push({"Name":"PROBES","Color":"#7B93A2"});

competencyNames.push({"Name":"COMMUNICATES WELL","Color":"#8DA9BF"});
competencyNames.push({"Name":"COMPLIES","Color":"#8DA9BF"});
competencyNames.push({"Name":"MAINTAINS ACCOUNTABILITY","Color":"#8DA9BF"});
competencyNames.push({"Name":"HAS RESEARCH SKILLS","Color":"#8DA9BF"});
competencyNames.push({"Name":"TROUBLESHOOTS","Color":"#8DA9BF"});


var tickStart = 6;
var tickEnd = 10;
 var rect = null;


var firstStart=1;
var firstEnd=3;
var topmargin=43;
var bottommargin=43;


var secondEnd=8;
var thirdEnd=15.5;

function calculatePixels(start,end,xAxis)
{
	
	  var pixStart = xAxis.toPixels (xAxis.tickPositions[start-1], false);
      var pixEnd = xAxis.toPixels (xAxis.tickPositions[(end*2)-1], false);
	
	var obj=[];
	obj.push(pixStart-topmargin);
	obj.push(pixEnd-topmargin);
	return obj;
}

function calculateCompentacyPixels(index,object,xAxis)
{
	
}


function cloneObject(obj) {
    var clone = {};
    for(var i in obj) {
        if(typeof(obj[i])=="object" && obj[i] != null)
            clone[i] = cloneObject(obj[i]);
        else
            clone[i] = obj[i];
    }
    return clone;
}


function drawRect(chart){
       if (rect){
           rect.element.remove();   
       }        

        //var xAxis = chart.xAxis[0];  
	var xAxis = chart.yAxis[1].chart.axes[0];  
	console.log(xAxis);
	console.log(chart);
	console.log(chart.yAxis);
	console.log(chart.xAxis);
	console.log("below is v");
	console.log(chart.xAxis[0]);
	
	
	var mappedobjs=chart.xAxis[0].options.categories.map(function(currentValue,index){
		
		if(currentValue.trim()!='')
		{
		
				console.log("category is"+currentValue+" and index is "+index);
			  	var pixStart = xAxis.toPixels (xAxis.tickPositions[index], false);
      			var pixEnd = xAxis.toPixels (xAxis.tickPositions[index+1], false);
	
			
			
		var mapped=	competencyNames.find(function(element){
				if(element.Name==currentValue.trim())
				{
					return true;
				}
			})
			
		if(mapped!=undefined)
		{
				var obj=[];
				obj.push(pixStart-topmargin);
				obj.push(pixEnd-topmargin);
				obj.push(mapped.Color);
				return obj;
		}
			

		
		}
	});
	
	console.log(mappedobjs);
	
	var axisWidth=chart.xAxis[0].chart.plotLeft;
	
        var pixStart = xAxis.toPixels (xAxis.tickPositions[tickStart], false);
        var pixEnd = xAxis.toPixels (xAxis.tickPositions[tickEnd], false);
	
	console.log("pix start"+pixStart);
	console.log(xAxis.tickPositions);
	//	chart.xAxis[1]=chart.xAxis[0];
	//chart.xAxis[1].opposite=true;
	console.log("pix end"+pixEnd);
    
	
	var firstrectpixels=calculatePixels(firstStart,firstEnd,xAxis);
	
	
	
	
	/*rect = chart.renderer.rect(0, topmargin, axisWidth, firstrectpixels[1], 0)
        .attr({
           'stroke-width': 0,
           stroke: '#395D8D',
           fill: '#395D8D',
           zIndex: 3
        })
        .add();
		
		var secondrectpixels=calculatePixels(firstEnd,secondEnd,xAxis);
	
	rect = chart.renderer.rect(0, firstrectpixels[1]+topmargin, axisWidth, secondrectpixels[1]- firstrectpixels[1]+2, 0)
        .attr({
           'stroke-width': 0,
           stroke: '#5C8ECA',
           fill: '#5C8ECA',
           zIndex: 3
        })
        .add();
		
		var thirdrectpixels=calculatePixels(secondEnd,thirdEnd,xAxis);
	
	rect = chart.renderer.rect(0, secondrectpixels[1]+topmargin, axisWidth, thirdrectpixels[1]-secondrectpixels[1]+bottommargin, 0)
        .attr({
           'stroke-width': 0,
           stroke: '#8DA9BF',
           fill: '#8DA9BF',
           zIndex: 3
        })
        .add();
	*/
	console.log("tpo margin "+topmargin);
	
	var filteredobjs=mappedobjs.filter(function(element)
	{
		if(element!=undefined )
			return true;
		return false;
	});
	
	var c=0;
	var totalcount=filteredobjs.length-1;
	for( var s in filteredobjs)
	{
		
		if(c==0)
		{
				rect = chart.renderer.rect(0,topmargin, axisWidth, filteredobjs[c][1]+2, 0)
        .attr({
           'stroke-width': 0,
           stroke: filteredobjs[c][2],
           fill: filteredobjs[c][2],
           zIndex: 3
        })
        .add();
		}
		else if(c==totalcount)
		{
				rect = chart.renderer.rect(0,filteredobjs[c-1][1]+topmargin, axisWidth, filteredobjs[c][0]+bottommargin+2, 0)
        .attr({
           'stroke-width': 0,
           stroke: filteredobjs[c][2],
           fill: filteredobjs[c][2],
           zIndex: 3
        })
        .add();
			
		}
		else{
				rect = chart.renderer.rect(0,filteredobjs[c-1][1]+topmargin, axisWidth, filteredobjs[c][1]-filteredobjs[c-1][1]+2, 0)
        .attr({
           'stroke-width': 0,
           stroke: filteredobjs[c][2],
           fill: filteredobjs[c][2],
           zIndex: 3
        })
        .add();
		}
		
		c++;
	}
	
	
	
	/*right sided background color*/
	console.log(chart.chartHeight);
	console.log("height width");
	console.log(chart.chartWidth);
	var startx=chart.chartWidth-chart.spacing[1];
c=0;
	
		for( var s in filteredobjs)
	{
		
		if(c==0)
		{
				rect = chart.renderer.rect(startx,topmargin, axisWidth, filteredobjs[c][1]+2, 0)
        .attr({
           'stroke-width': 0,
           stroke: filteredobjs[c][2],
           fill: filteredobjs[c][2],
           zIndex: 3
        })
        .add();
		}
		else if(c==totalcount)
		{
				rect = chart.renderer.rect(startx,filteredobjs[c-1][1]+topmargin, axisWidth, filteredobjs[c][0]+bottommargin+2, 0)
        .attr({
           'stroke-width': 0,
           stroke: filteredobjs[c][2],
           fill: filteredobjs[c][2],
           zIndex: 3
        })
        .add();
			
		}
		else{
				rect = chart.renderer.rect(startx,filteredobjs[c-1][1]+topmargin, axisWidth, filteredobjs[c][1]-filteredobjs[c-1][1]+2, 0)
        .attr({
           'stroke-width': 0,
           stroke: filteredobjs[c][2],
           fill: filteredobjs[c][2],
           zIndex: 3
        })
        .add();
		}
		
		c++;
	}

	
	
	

	
	
	
	
	
	
	
	
	
	/*
	
	
	
		rect = chart.renderer.rect(startx, topmargin, axisWidth, firstrectpixels[1], 0)
        .attr({
           'stroke-width': 0,
           stroke: '#395D8D',
           fill: '#395D8D',
           zIndex: 3
        })
        .add();
		
		var secondrectpixels=calculatePixels(firstEnd,secondEnd,xAxis);
	
	rect = chart.renderer.rect(startx, firstrectpixels[1]+topmargin, axisWidth, secondrectpixels[1]- firstrectpixels[1]+2, 0)
        .attr({
           'stroke-width': 0,
           stroke: '#5C8ECA',
           fill: '#5C8ECA',
           zIndex: 3
        })
        .add();
		
		var thirdrectpixels=calculatePixels(secondEnd,thirdEnd,xAxis);
	
	rect = chart.renderer.rect(startx, secondrectpixels[1]+topmargin, axisWidth, thirdrectpixels[1]-secondrectpixels[1]+bottommargin, 0)
        .attr({
           'stroke-width': 0,
           stroke: '#8DA9BF',
           fill: '#8DA9BF',
           zIndex: 3
        })
        .add();*/
	
	/* rect = chart.renderer.rect(startx, 43, 123, 150, 0)
        .attr({
           'stroke-width': 0,
           stroke: '#395D8D',
           fill: '#395D8D',
           zIndex: 3
        })
        .add();
		
	
	rect = chart.renderer.rect(startx, 193, 123, 260, 0)
        .attr({
           'stroke-width': 0,
           stroke: '#5C8ECA',
           fill: '#5C8ECA',
           zIndex: 3
        })
        .add();
		
	
	
	rect = chart.renderer.rect(startx, 453, 123, 153, 0)
        .attr({
           'stroke-width': 0,
           stroke: '#8DA9BF',
           fill: '#8DA9BF',
           zIndex: 3
        })
        .add();
		*/
	
	
	
    }



widget.on('render', function(widget,event){
    //  Define how each value series is used
    var displaySeriesIndex = 0,
        labelSeriesIndex = 1;
    //  Define where to put the center line
    var centerLineValue = 0;
    
    //  Get a reference to the highcharts object
    var chart = widget.queryResult; 
    //  Define a centerline to add to the chart
    var plotline = {
        color: 'black',
        dashStyle: 'solid',
        width: 2,
        value: centerLineValue
    }
	
	var plotBands= [{
    color: '#f3f3f3', // Color value
    from: 0.68, // Start of the plot band
    to: 1, // End of the plot band
		label: { 
    text: 'Leads standard', // Content of the label. 
    align: 'center' ,// Positioning of the label. 
			x:+30,
		
			style:{fontWeight:'bold',fontSize:'15px'}

    
  }
  },
	{
    color: '#fff', // Color value
    from:0.33, // Start of the plot band
    to: 0.495 , // End of the plot band
		label: { 
    text: 'Meets standard', // Content of the label. 
    align: 'center' ,// Positioning of the label. 
    x:+80,
	style:{fontWeight:'bold',fontSize:'15px'}
  }
  },
{
    color: '#fff', // Color value
    from:0.505, // Start of the plot band
    to: 0.68  // End of the plot band
		
  },				
{
    color: '#f3f3f3', // Color value
    from: 0, // Start of the plot band
    to: 0.32, // End of the plot band
		label: { 
    text: 'Lags standard', // Content of the label. 
    align: 'center', // Positioning of the label. 
    	x:-30,
	style:{fontWeight:'bold',fontSize:'15px'}
  }
  }]
	
	
   
	
	window.chart=chart;
	
	chart.chart.events.load=function(chart) {
                  
		drawRect(this);
                }
	
	chart.chart.events.redraw=function() {
                    drawRect(this);
                }
	
	
	
	
	
    // Get a reference to the series
    var displaySeries = chart.series[displaySeriesIndex],
        labelSeries = chart.series[labelSeriesIndex];
    //  Loop through each data point in then display series
	
	if(displaySeries != undefined)
	{
		
		
	 //  Add the center line to the series
   chart.yAxis[0].plotLines = [];
		chart.yAxis[0].plotLines.push(plotline);
	chart.yAxis[1].plotBands=plotBands;
	console.log("plot line is below");
		console.log(chart.yAxis[0].plotLines);	
		
	console.log(displaySeries);
	console.log("Loop through each data point in then display series");
    displaySeries.data.forEach(function(data, index){
        //  Save a reference to the label value
 		if(labelSeries) {
        data.labelValue = labelSeries.data[index].y;
		}
    })
    //  Remove the label series
	console.log("Remove the label series");
    chart.series.splice(labelSeriesIndex,1);
    //  Get a reference to the sisense formatter
    var numberFormatter = prism.$injector.get('$filter')('numeric');
    //  Get a reference to the formatting mask of the label's series
    var mask = $$get(widget.metadata.panel('values').items[labelSeriesIndex], 'format.mask', {});
    //  Override the label formatter function
    chart.plotOptions.series.dataLabels.formatter = function(){
        //  Use the formatting of the label series, in order to create the value label
        var newLabel = numberFormatter(this.point.options.labelValue, mask);
        return newLabel;
    }
	
	}

	
})



widget.on("beforedatapointtooltip", function(w, args){
 args.template = args.template.replace("{{item.value}}", "{{(item.value*100)+50}}"+"%");
	
});


/*************************************************/
/********Change the Axis Labels Formatting********/
/*************************************************/ 

widget.on('render', function(se,ev){
 
      /*Change the X Axis Labels' Rotation, Distance from Axis, Color, Font Size, Font Weight, and Style*/

 //se.queryResult.xAxis.labels.rotation = -45;
 //se.queryResult.xAxis.labels.y = 30;
 se.queryResult.xAxis.labels.style.color = "#FFFFFF";
// se.queryResult.xAxis.labels.style.minWidth= "450px";
//se.queryResult.xAxis.labels.style.maxWidth = "450px";
//		se.queryResult.xAxis.labels.style.width = "450px";
 //se.queryResult.xAxis.labels.useHTML = true
 se.queryResult.series[0].data[0].dataLabels.style.width="250px";
 
 ///se.queryResult.xAxis.labels.style.fontSize = 15;
/// se.queryResult.xAxis.labels.style.fontWeight = "bold";
//se.queryResult.xAxis.labels.style.fill = "#58C952";
 //se.queryResult.xAxis.labels.style.fontFamily = "Verdana";
//	se.queryResult.xAxis.labels.style.backgroundColor = "violet";
// se.queryResult.yAxis[0].labels.rotation = 45;
// se.queryResult.yAxis[0].labels.x = 15;
// se.queryResult.yAxis[0].labels.style.color = "#ffcb05";
 se.queryResult.yAxis[1].labels.style.fontSize = 15;
se.queryResult.yAxis[1].labels.style.fontWeight = "bold";
// se.queryResult.yAxis[0].labels.style.fontFamily = "Arial";
 console.log("below");
console.log(se.queryResult);
	se.queryResult.xAxis.plotBands.forEach(function(item){
	item.label.style.color= "#FFFFFF";
	});
	
	
	
	 se.queryResult.yAxis[0].labels.style.minWidth= "450px";
	se.queryResult.yAxis[0].labels.style.maxWidth = "450px";
		se.queryResult.yAxis[0].labels.style.width = "450px";
 se.queryResult.yAxis[0].labels.useHTML = true
	
	
	
	

	}) 

widget.on('ready', function(se, ev){

    $(element).find('.highcharts-series-group').find('rect').attr('width','15');
	$('.highcharts-axis-labels').attr('width','15');
	console.log($('.highcharts-axis-labels'));
	$('.highcharts-plot-band-label').attr('text-anchor','start');
})

/******* user configuration **********/ 
var seriesName = "Score"; 
/*************************************/ 
widget.on('processresult', function(sender, ev){ 

var data = _.find(ev.result.series, function (ser) {return ser.name == seriesName}).data 
_.each(data, function(value){ 
//enable dataLabels, change its font, make it bold and rotate by 270 
	debugger;
	if (value.y < 0) {
value.dataLabels = {enabled:true, style:{'fontSize':'14px', 'fontWeight':'bold'}, rotation: 0, align:'right'} 
	} else {
		value.dataLabels = {enabled:true, style:{'fontSize':'14px', 'fontWeight':'bold'}, rotation: 0, align:'left'} 
	}
}) ;
	
	
})


	



